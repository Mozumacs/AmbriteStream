//
//  PlayerView.swift
//  AmbriteStream
//
//  Created by Janis Mozumacs on 28/04/2018.
//  Copyright © 2018 Janis. All rights reserved.
//

import UIKit
import AVKit


class PlayerView: UIView {

    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var playerItem: AVPlayerItem?

    func set(playerItem: AVPlayerItem){
        if (player != nil) {
            player.replaceCurrentItem(with: playerItem)
        }else{
            player = AVPlayer.init(playerItem: playerItem)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            playerLayer.frame = self.bounds
            layer.addSublayer(playerLayer)
        }
    }
}
