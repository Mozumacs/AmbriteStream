//
//  FirstViewController.swift
//  AmbriteStream
//
//  Created by Janis on 25/04/2018.
//  Copyright © 2018 Janis. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

class StreamViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerControlsView: UIView!
    @IBOutlet weak var videoView: PlayerView!
    @IBOutlet weak var resolutionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var peakBitrate: UILabel!
    
    //Variables
    var streamsList: [String] = []
    var playedVideoIndex = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadStreams()
        setupAirplayButton()
        activityIndicator.startAnimating()
        setupTimerToHideControls()
        
        //setup gesture
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.videoView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.videoView.addGestureRecognizer(swipeRight)
        
        let videoTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleVideoTap))
        self.videoView.addGestureRecognizer(videoTapGesture)

    }
    
    func downloadStreams(){
        guard let requestUrl = URL(string:Constants.streamsURL) else { return }
        let request = URLRequest(url:requestUrl)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let unwrappedData = data else { return }
            do {
                if let json = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? [[String: String]]{
                    for url in json {
                        if let urlString = url["url"] {
                            self.streamsList.append(urlString)
                        }
                    }
                    DispatchQueue.main.async {
                        if (self.streamsList.count > 0){
                            self.initializeVideoPlayer(withUrl: self.streamsList.first!)
                        }else{
                            self.initializeVideoPlayer(withUrl: Constants.defaultURL)
                        }
                    }
                }
                
            } catch {
                print("json error: \(error)")
                DispatchQueue.main.async {
                    self.initializeVideoPlayer(withUrl: Constants.defaultURL)
                }
            }
        }
        task.resume()
    }
    
    func initializeVideoPlayer(withUrl: String) {
        
        guard let url = URL(string: withUrl) else {
            return
        }
        
        activityIndicator.startAnimating()
        
        let playerItem = AVPlayerItem(url: url)
        videoView.set(playerItem: playerItem)
        videoView.player.currentItem?.addObserver(self, forKeyPath: "status", context: nil)
        videoView.player.currentItem?.addObserver(self, forKeyPath: "rate", context: nil)
    }
    
    override func observeValue(forKeyPath: String?, of: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if forKeyPath == "rate"{
            playButton.isHidden = false
            if videoView.player.timeControlStatus == AVPlayerTimeControlStatus.playing{
                playButton.isSelected = true
            }
            else if videoView.player.timeControlStatus == AVPlayerTimeControlStatus.paused{
                playButton.isSelected = false
            }
        }
        if forKeyPath == "status"{
            if videoView.player.currentItem?.status == AVPlayerItemStatus.readyToPlay{
                activityIndicator.stopAnimating()
                playButton.isSelected = true
                videoView.player.play()
                displayStreamInfo()
            }
        }
    }
    
    func displayStreamInfo(){
        //resolution
        let videoRect = videoView.playerLayer.videoRect
        let width = String(format: "%.0f", videoRect.size.width)
        let height = String(format: "%.0f", videoRect.size.height)
        resolutionLabel.text = "\(width) * \(height)"
        
        //duration
        if let duration = videoView.player.currentItem?.asset.duration{
            let minutes = CMTimeGetSeconds(duration) / 60
            durationLabel.text = "\(String(format: "%.2f",minutes)) minutes"
        }
        
        
    }
    
    func setupAirplayButton(){
        let rect = CGRect(x: 10, y: 10, width: 50, height: 50)
        if #available(iOS 11.0, *) {
           let routepicker = AVRoutePickerView.init(frame: rect)
            playerControlsView.addSubview(routepicker)
        } else {
            let volumeView = MPVolumeView.init(frame: rect)
            playerControlsView.addSubview(volumeView)
        }
    }
    
    @IBAction func playButtonTouched(_ sender: UIButton) {
        if videoView.player.timeControlStatus == AVPlayerTimeControlStatus.playing{
            videoView.player.pause()
            sender.isSelected = false
        }else if videoView.player.timeControlStatus == AVPlayerTimeControlStatus.paused{
            videoView.player.play()
            sender.isSelected = true
        }
    }
    
    //timer action
    @objc func timerAction(){
        playerControlsView.isHidden = true;
    }
    
    func setupTimerToHideControls(){
        timer.invalidate()
        playerControlsView.isHidden = false
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    //video tap
    @objc func handleVideoTap(gesture: UITapGestureRecognizer) -> Void {
        setupTimerToHideControls()
    }
    
    //swipe
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        playerControlsView.isHidden = true
    
        
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            if playedVideoIndex > 0{
                playedVideoIndex = playedVideoIndex - 1
            }
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            if playedVideoIndex < streamsList.count - 1{
                playedVideoIndex = playedVideoIndex + 1
            }
        }
        initializeVideoPlayer(withUrl: streamsList[playedVideoIndex])
    }
}

