//
//  SecondViewController.swift
//  AmbriteStream
//
//  Created by Janis on 25/04/2018.
//  Copyright © 2018 Janis. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var appVersionlabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.appVersionlabel.text = version
        }
    }
}

