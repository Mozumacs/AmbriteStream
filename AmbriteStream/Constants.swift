//
//  Constants.swift
//  AmbriteStream
//
//  Created by Janis Mozumacs on 28/04/2018.
//  Copyright © 2018 Janis. All rights reserved.
//

import UIKit

struct Constants{

    static var streamsURL = "https://sheetsu.com/apis/v1.0su/25e301477e2c"

    static var defaultURL = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
}
